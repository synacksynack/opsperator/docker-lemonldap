SKIP_SQUASH?=1
FRONTNAME=opsperator
IMAGE=opsperator/lemonldap
-include Makefile.cust

.PHONY: build
build:
	SKIP_SQUASH=$(SKIP_SQUASH) hack/build.sh

.PHONY: demo
demo: run

.PHONY: run
run:
	@@MAINDEV=`ip r | awk '/default/{print $$0;exit;}' | sed 's|.* dev \([^ ]*\).*|\1|'`; \
	MAINIP=`ip r | awk "/ dev $$MAINDEV .* src /" | sed 's|.* src \([^ ]*\).*$$|\1|'`; \
	docker run -e OPENLDAP_HOST=$$MAINIP \
	    -e DEBUG=pleasedo \
	    -e LLNG_PUB_PORT=8080 \
	    -e OPENLDAP_PORT=1389 \
	    --add-host=openldap:$$MAINIP \
	    --add-host=auth.demo.local:127.0.0.1 \
	    --add-host=manager.demo.local:127.0.0.1 \
	    --add-host=reload.demo.local:127.0.0.1 \
	    -p 8080:8080 $(IMAGE)

.PHONY: themeddemo
themeddemo:
	@@MAINDEV=`ip r | awk '/default/{print $$0;exit;}' | sed 's|.* dev \([^ ]*\).*|\1|'`; \
	MAINIP=`ip r | awk "/ dev $$MAINDEV .* src /" | sed 's|.* src \([^ ]*\).*$$|\1|'`; \
	docker run -e OPENLDAP_HOST=$$MAINIP \
	    -e LEMON_CUSTOM_THEMES=git+ssh://some.private.git/Project/llng-themes-repo \
	    -e LLNG_PUB_PORT=8080 \
	    -e OPENLDAP_PORT=1389 \
	    -e DEBUG=pleasedo -e HOME=/ -e GIT_SSH_PORT=2222 \
	    --add-host=openldap:$$MAINIP \
	    --add-host=auth.demo.local:127.0.0.1 \
	    --add-host=manager.demo.local:127.0.0.1 \
	    --add-host=reload.demo.local:127.0.0.1 \
	    -p 8080:8080 $(IMAGE)

.PHONY: checkicons
checkicons:
	@@if ! test -d ../docker-openldap; then \
	    echo Make sure to clone docker-openldap in parent directory first >&2; \
	    exit 1; \
	fi; \
	grep -E '(png|gif|jpg)' ../docker-openldap/config/llng-upgrade-helpers.sh \
	    | sed 's|.*"\([^"]*\.[jpg][pni][gf]\)\\".*|\1|' \
	    | sort -u | while read icon; \
	    do if test $$icon = database.png; then \
		continue; \
	    elif ! test -s ./config/images/$$icon; then \
		echo missing $$icon; \
	    fi; \
	done

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in secret service configmap statefulset deployment; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: ocbuild
ocbuild: occheck
	@@oc process -f deploy/openshift/imagestream.yaml | oc apply -f-
	@@BRANCH=`git rev-parse --abbrev-ref HEAD`; \
	if test "$$GIT_DEPLOYMENT_TOKEN"; then \
	    oc process -f deploy/openshift/build-with-secret.yaml \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_DEPLOYMENT_TOKEN" \
		-p "LLNG_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	else \
	    oc process -f deploy/openshift/build.yaml \
		-p "LLNG_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	fi

.PHONY: occheck
occheck:
	@@oc whoami >/dev/null 2>&1 || exit 42

.PHONY: occlean
occlean: occheck
	@@oc process -f deploy/openshift/run-ha.yaml -p FRONTNAME=$(FRONTNAME) \
	    | oc delete -f- || true
	@@oc process -f deploy/openshift/run-persistent.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true
	@@oc process -f deploy/openshift/secret.yaml -p FRONTNAME=$(FRONTNAME) \
	    | oc delete -f- || true

.PHONY: ocdemoephemeral
ocdemoephemeral: ocbuild
	@@if ! oc describe secret openldap-$(FRONTNAME) >/dev/null 2>&1; then \
	    oc process -f deploy/openshift/secret.yaml \
		-p FRONTNAME=$(FRONTNAME) | oc apply -f-; \
	fi
	@@oc process -f deploy/openshift/run-ephemeral.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc apply -f-

.PHONY: ocdemopersistent
ocdemopersistent: ocbuild
	@@if ! oc describe secret openldap-$(FRONTNAME) >/dev/null 2>&1; then \
	    oc process -f deploy/openshift/secret.yaml \
		-p FRONTNAME=$(FRONTNAME) | oc apply -f-; \
	fi
	@@oc process -f deploy/openshift/run-persistent.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc apply -f-

.PHONY: ocdemo
ocdemo: ocdemoephemeral

.PHONY: ocprod
ocprod: ocbuild
	@@if ! oc describe secret openldap-$(FRONTNAME) >/dev/null 2>&1; then \
	    oc process -f deploy/openshift/secret-prod.yaml \
		-p FRONTNAME=$(FRONTNAME) | oc apply -f-; \
	fi
	@@oc process -f deploy/openshift/run-ha.yaml -p FRONTNAME=$(FRONTNAME) \
	    | oc apply -f-

.PHONY: ocpurge
ocpurge: occlean
	@@oc process -f deploy/openshift/build.yaml | oc delete -f- || true
	@@oc process -f deploy/openshift/imagestream.yaml | oc delete -f- || true
