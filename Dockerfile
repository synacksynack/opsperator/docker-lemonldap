FROM opsperator/apache

# LemonLDAP-NG image for OpenShift Origin

ARG DO_UPGRADE=
ENV SKINS_ROOT=/usr/share/lemonldap-ng/portal

LABEL io.k8s.description="LemonLDAP::NG offers a full AAA (Authentication Authorization Accounting) protection" \
      io.k8s.display-name="LemonLDAP::NG 2.0.15" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="sso,lemon,lemonldap,llng,llng2" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-lemonldap" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="2.0.15"

USER root

COPY config /

RUN set -ex \
    && rm -rf /var/lib/apt/lists/* \
    && echo "# Install LemonLDAP::NG configuration sample" \
    && mkdir -p /etc/lemonldap-ng \
    && cp -p /root/original-lemonldap-ng.ini \
	/etc/lemonldap-ng/lemonldap-ng.ini \
    && apt-get -y update \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install LemonLDAP::NG dependencies" \
    && apt-get -y install libglib-perl wget git libemail-sender-perl \
	libnet-ldap-perl libcache-cache-perl libdbi-perl perl-modules \
	libwww-perl libxml-simple-perl libsoap-lite-perl libhtml-template-perl \
	libregexp-assemble-perl libregexp-common-perl libjs-jquery \
	libxml-libxml-perl libcrypt-rijndael-perl libio-string-perl \
	libxml-libxslt-perl libconfig-inifiles-perl libunicode-string-perl \
	libstring-random-perl libemail-date-format-perl libmime-lite-perl \
	libcrypt-openssl-rsa-perl libdigest-hmac-perl libdigest-sha-perl \
	libclone-perl libauthen-sasl-perl libnet-cidr-lite-perl liblasso-perl \
	libcrypt-openssl-x509-perl libauthcas-perl libtest-pod-perl \
	libtest-mockobject-perl libauthen-captcha-perl openssh-client \
	libnet-openid-consumer-perl libnet-openid-server-perl libdbd-pg-perl \
	libmoose-perl libconvert-pem-perl libplack-perl libjson-perl \
    && echo "# Install LemonLDAP::NG" \
    && echo Y | apt-get -y install libapache2-mod-fcgid lemonldap-ng \
    && rm -fr /etc/apache2/sites-available/*apache2.conf \
	/etc/lemonldap-ng/*apache2.conf /etc/lemonldap-ng/*nginx.conf \
	/etc/lemonldap-ng/lemonldap-ng.ini /usr/share/lemon \
    && mv /handler.conf /manager.conf /portal.conf /lemonldap-ng.ini \
	/etc/lemonldap-ng/ \
    && mv /WSweet.pm /usr/share/perl5/Lemonldap/NG/Portal/Register/ \
    && mv /WSweetIDPSelector.pm /usr/share/perl5/Lemonldap/NG/Portal/Plugins/ \
    && mv /wsweetidpselector.tpl \
	/usr/share/lemonldap-ng/portal/templates/bootstrap/ \
    && echo "# Backup Original Configuration" \
    && mkdir /usr/share/lemon \
    && mv /etc/lemonldap-ng /usr/share/lemon/etc-lemonldap-ng \
    && mv /var/lib/lemonldap-ng/conf \
	/usr/share/lemon/var-lib-lemonldap-ng-conf \
    && echo "# Configure Apache" \
    && a2dismod mpm_event \
    && a2enmod fcgid mpm_prefork \
    && echo "FcgidIPCDir /etc/lemonldap-ng/mod_fcgid" \
	>>/etc/apache2/mods-available/fcgid.conf \
    && echo "FcgidProcessTableFile /etc/lemonldap-ng/mod_fcgid/fcgid_shm" \
	>>/etc/apache2/mods-available/fcgid.conf \
    && echo "# Install Custom themes" \
    && mkdir -p /var/lib/lemonldap-ng/portal/skins \
    && for skin in wsweet kubelemon custom; \
	do \
	    ( \
		mkdir -p $SKINS_ROOT/templates/$skin \
		    $SKINS_ROOT/htdocs/static/$skin \
		&& cd $SKINS_ROOT/templates/$skin/ \
		&& cp -rp ../bootstrap/* . \
		&& cd ../../htdocs/static/$skin/ \
		&& cp -rp ../bootstrap/* . \
		&& ln -sf $SKINS_ROOT/templates/$skin \
		    /var/lib/lemonldap-ng/portal/skins/; \
	    ) && for f in header footer matomo; \
		do \
		    if test -s "/$skin$f.tpl"; then \
			mv "/$skin$f.tpl" \
			    "$SKINS_ROOT/templates/$skin/custom$f.tpl"; \
		    elif test -s "/custom$f.tpl"; then \
			cp "/custom$f.tpl" \
			    "$SKINS_ROOT/templates/$skin/custom$f.tpl"; \
		    fi \
		done \
	    && if test -s "/logo_$skin.png"; then \
		mv "/logo_$skin.png" \
		    "$SKINS_ROOT/htdocs/static/$skin/logo.png"; \
	    fi; \
	done \
    && mv /images/* "$SKINS_ROOT/htdocs/static/common/apps/" \
    && mv /wall*.jpg "$SKINS_ROOT/htdocs/static/common/backgrounds/" \
    && mkdir /.ssh \
    && echo "# Fixing permissions" \
    && chmod 0664 /etc/apache2/mods-available/fcgid.conf \
    && chown 1001:root /etc/apache2/mods-available/fcgid.conf \
    && chmod 0770 /.ssh \
    && chown 1001:root /.ssh \
    && for dir in $SKINS_ROOT/templates/custom /etc/lemonldap-ng \
	$SKINS_ROOT/templates/kubelemon $SKINS_ROOT/templates/wsweet \
	$SKINS_ROOT/htdocs/static/custom /var/lib/lemonldap-ng/notifications; \
	do \
	    mkdir -p $dir 2>/dev/null \
	    && chown -R 1001:root "$dir" \
	    && chmod -R g=u "$dir"; \
	done \
    && echo "# Cleaning up" \
    && apt-get autoremove -y --purge \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
	/etc/ssh/ssh_config /tmp/lemonldap-ng-config /theme \
	/etc/apache2/sites-enabled/*default* /images /*.tpl /images \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

CMD "/usr/sbin/apache2ctl" "-D" "FOREGROUND"
ENTRYPOINT [ "dumb-init", "--", "/run-lemonldap.sh" ]
USER 1001
