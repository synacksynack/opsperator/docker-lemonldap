package Lemonldap::NG::Portal::Plugins::WSweetIDPSelector;

use Mouse;
use Lemonldap::NG::Portal::Main::Constants qw(PE_OK PE_REDIRECT PE_ERROR);
extends 'Lemonldap::NG::Portal::Main::Plugin';

use constant beforeAuth => 'catchUser';

sub init {
    my ($self) = @_;
    $self->addUnauthRoute( wsweetidpselector => 'select', [ 'GET', 'POST' ] );
    return 1;
}

sub catchUser {
    my ( $self, $req ) = @_;

    my $idp = $req->param("idp");

    if ($idp) {
        $self->p->logger->debug(
"[WSweetIDPSelector] An IDP was selected: $idp. Continue authentication process."
        );

        return PE_OK;
    }

    # Redirect on selection page
    $req->urldc("/wsweetidpselector");

    return PE_REDIRECT;

}

sub select {
    my ( $self, $req ) = @_;

    my $tplParams = {
        MAIN_LOGO     => $self->conf->{portalMainLogo},
        HIDDEN_INPUTS => $self->p->buildHiddenForm($req),
    };

    # Test if mail submitted
    my $mail = $req->param("mail");

    if ($mail) {

        $self->p->logger->debug(
            "[WSweetIDPSelector] User submitted mail $mail");

        # Parse email
        my ( $user, $domain ) = ( $mail =~ /([^@]+)@(.+)/ );

        unless ( $user and $domain ) {
            $self->p->logger->error("[WSweetIDPSelector] Email is not valid");

            $tplParams->{"WSWEET_ERROR_EMAILNOTVALID"} = 1;

            return $self->p->sendHtml( $req, 'wsweetidpselector',
                params => $tplParams );
        }

        # Browse Wsweet domains
        my $idp;
        foreach
          my $wsweetDomain ( keys %{ $self->conf->{customPluginsParams} } )
        {
            next unless ( $wsweetDomain =~ /^wsweetDomain/ );
            if ( $self->conf->{customPluginsParams}->{$wsweetDomain} =~
                /^$domain$/i )
            {
                ($idp) = ( $wsweetDomain =~ /wsweetDomain_(.+)/ );
                $self->p->logger->debug(
                    "[WSweetIDPSelector] Domain $domain match IDP $idp");
                last;
            }
        }

        unless ($idp) {
            $self->p->logger->error(
                "[WSweetIDPSelector] No WSweet domain found for $domain");

            $tplParams->{"WSWEET_ERROR_NOWSWEETDOMAIN"} = 1;

            return $self->p->sendHtml( $req, 'wsweetidpselector',
                params => $tplParams );
        }

        $self->p->logger->debug("[WSweetIDPSelector] Use IDP $idp");

        # Go back to portal with choosen IDP
        $req->urldc( $self->conf->{portal} . "?idp=$idp" );
        return $self->p->do( $req, [ sub { PE_REDIRECT } ] );
    }

    return $self->p->sendHtml( $req, 'wsweetidpselector',
        params => $tplParams );
}

1;
