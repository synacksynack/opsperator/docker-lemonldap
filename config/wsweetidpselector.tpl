<TMPL_INCLUDE NAME="header.tpl">
<div class="container">
    <div class="card">
	<div class="card-header text-white text-center bg-secondary">
	    <h4 class="text-center card-title"><span trspan="wsweetIDPSelection"></span></h4>
	</div>
	<div class="card-body">
	    <TMPL_IF NAME="WSWEET_ERROR_EMAILNOTVALID">
	    <div class="alert alert-danger" role="alert">
		<span trspan="wsweetEmailNotValid"></span>
	    </div>
	    </TMPL_IF>
	    <TMPL_IF NAME="WSWEET_ERROR_NOWSWEETDOMAIN">
	    <div class="alert alert-danger" role="alert">
		<span trspan="wsweetNoWSweetDomain"></span>
	    </div>
	    </TMPL_IF>
	    <div class="alert alert-info" role="alert">
		<span trspan="wsweetEnterYourEmail"></span>
	    </div>
	    <form method="post" class="login">
		<!-- Hidden fields -->
		<TMPL_VAR NAME="HIDDEN_INPUTS">
		<input type="hidden" name="url" value="<TMPL_VAR NAME="AUTH_URL">" />
		<input type="hidden" name="timezone" />
		<input type="hidden" name="<TMPL_VAR NAME="CHOICE_PARAM">" value="<TMPL_VAR NAME="key">" />
		<input type="hidden" name="skin" value="<TMPL_VAR NAME="SKIN">" />
		<TMPL_IF NAME="TOKEN">
		<input type="hidden" name="token" value="<TMPL_VAR NAME="TOKEN">" />
		</TMPL_IF>
		<div class="input-group mb-3">
		    <div class="input-group-prepend">
			<span class="input-group-text"><i class="fa fa-envelope"></i> </span>
		    </div>
		    <input name="mail" type="text" value="<TMPL_VAR NAME="MAIL">" class="form-control" trplaceholder="mail" required aria-required="true"/>
		</div>
		<button type="submit" class="btn btn-success">
		    <span class="fa fa-envelope-open"></span>
		    <span trspan="submit">Submit</span>
		</button>
	    </form>
	</div>
    </div>
</div>
<TMPL_INCLUDE NAME="footer.tpl">
