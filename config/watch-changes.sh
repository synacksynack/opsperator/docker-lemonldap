#!/bin/sh

. /main-init.sh

unset LEMON_CUSTOM_THEMES APACHE_LOG_LEVEL APACHE_DOMAIN \
    PUBLIC_PROTO SSL_INCLUDE

get_last_config_id() {
    ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
	-D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
	-b "$OPENLDAP_CONF_DN_PREFIX,$OPENLDAP_BASE" \
	-w "$OPENLDAP_BIND_PW" "(objectClass=applicationProcess)" cn \
	2>/dev/null | awk '/^cn:/{print $2}' | cut -d- -f2 | sort -n | tail -1
}

OLDCFG=`get_last_config_id`
RELOAD_INTERVAL=${RELOAD_INTERVAL:-300}
while sleep $RELOAD_INTERVAL
do
    CFGVERS=`get_last_config_id`
    if test "$CFGVERS" != "$OLDCFG"; then
	echo Reloading LLNG configuration
	wget http://127.0.0.1:$APACHE_HTTP_PORT/reload \
	    --header "Host: reload.$OPENLDAP_DOMAIN"
	rm -f reload
	OLDCFG="$CFGVERS"
    fi
done

exit 0
