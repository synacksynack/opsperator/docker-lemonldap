if test "$DEBUG"; then
    APACHE_LOG_LEVEL=perl:debug
    LLNG_LOG_LEVEL=debug
    set -x
else
    APACHE_LOG_LEVEL=notice
    LLNG_LOG_LEVEL=notice
fi
. /usr/local/bin/nsswrapper.sh

APACHE_HTTP_PORT=${APACHE_HTTP_PORT:-8080}
OPENLDAP_BIND_DN_PREFIX="${OPENLDAP_BIND_DN_PREFIX:-cn=lemonldap,ou=services}"
OPENLDAP_BIND_PW="${OPENLDAP_BIND_PW:-secret}"
OPENLDAP_CONF_DN_PREFIX="${OPENLDAP_CONF_DN_PREFIX:-ou=lemonldap,ou=config}"
OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
OPENLDAP_HOST=${OPENLDAP_HOST:-127.0.0.1}
OPENLDAP_PROTO=${OPENLDAP_PROTO:-ldap}
PUBLIC_PROTO=${PUBLIC_PROTO:-http}
test -z "$LLNG_APPNAME" && LLNG_APPNAME=KubeLemon
if test -z "$OPENLDAP_BASE"; then
    OPENLDAP_BASE=`echo "dc=$OPENLDAP_DOMAIN" | sed 's|\.|,dc=|g'`
fi
if test -z "$OPENLDAP_PORT" -a "$OPENLDAP_PROTO" = ldaps; then
    OPENLDAP_PORT=636
elif test -z "$OPENLDAP_PORT"; then
    OPENLDAP_PORT=389
fi
if test -z "$APACHE_DOMAIN"; then
    APACHE_DOMAIN=auth.$OPENLDAP_DOMAIN
fi
export APACHE_DOMAIN
export APACHE_HTTP_PORT
export OPENLDAP_BASE
export OPENLDAP_BIND_DN_PREFIX
export OPENLDAP_DOMAIN
export OPENLDAP_HOST
export PUBLIC_PROTO
SSL_INCLUDE=no-ssl
. /usr/local/bin/reset-tls.sh
export RESET_TLS=false

if test -s /etc/apache2/ssl/server.key; then
    DEF_PORT=443
elif test "$PUBLIC_PROTO" = https; then
    DEF_PORT=443
else
    DEF_PORT=80
fi
if test "$LLNG_PUB_PORT"; then
    if test "$LLNG_PUB_PORT" = "$DEF_PORT"; then
	LLNG_PUB_PORT=
    else
	LLNG_PUB_PORT=":$LLNG_PUB_PORT"
    fi
fi
unset DEF_PORT

echo "Install LemonLDAP-NG Service Configuration"
ls /usr/share/lemon/etc-lemonldap-ng/ 2>/dev/null | while read conf
    do
	sed -e "s LDAP_PROTO $OPENLDAP_PROTO g" \
	    -e "s LDAP_HOST $OPENLDAP_HOST g" \
	    -e "s DEBUG $LLNG_LOG_LEVEL g" \
	    -e "s HTTP_PORT $APACHE_HTTP_PORT g" \
	    -e "s LOG_LEVEL $APACHE_LOG_LEVEL g" \
	    -e "s LDAP_PORT $OPENLDAP_PORT g" \
	    -e "s|LDAP_SUFFIX|$OPENLDAP_BASE|g" \
	    -e "s LDAP_DOMAIN $OPENLDAP_DOMAIN g" \
	    -e "s|LDAP_CONF_DN_PREFIX|$OPENLDAP_CONF_DN_PREFIX|g" \
	    -e "s|LDAP_BIND_DN_PREFIX|$OPENLDAP_BIND_DN_PREFIX|g" \
	    -e "s|LDAP_BIND_PW|$OPENLDAP_BIND_PW|g" \
	    -e "s|HTTP_PUB_PROTO|$PUBLIC_PROTO|g" \
	    -e "s|:HTTP_PUB_PORT|$LLNG_PUB_PORT|g" \
	    -e "s SSL_TOGGLE_INCLUDE /etc/apache2/$SSL_INCLUDE.conf g" \
	    "/usr/share/lemon/etc-lemonldap-ng/$conf" >"/etc/lemonldap-ng/$conf"
    done

if test "$IDP_SELECTOR_DOMAINS"; then
    for domain in $IDP_SELECTOR_DOMAINS
    do
	cname=$(echo $domain | cut -d. -f1)
	test "$params" && params="$params," || params="{"
	params="$params 'wsweetDomain_$cname' => '$domain'"
    done
    params="$params }"
	cat <<EOF >>/etc/lemonldap-ng/lemonldap-ng.ini
[portal]
customPlugins = Lemonldap::NG::Portal::Plugins::WSweetIDPSelector
customPluginsParams = $params
msg_wsweetIDPSelection = $LLNG_APPNAME Identity Provider selection
msg_fr_wsweetIDPSelection = Sélection du fournisseur d'identité $LLNG_APPNAME
msg_wsweetEnterYourEmail = Please enter your email, you will the be redirected to your Identity Provider. You will need to authenticate on this Identity Provider to open your $LLNG_APPNAME session.
msg_fr_wsweetEnterYourEmail = Entrez votre adresse email, vous serez alors redirigé vers votre fournisseur d'identité. Vous devrez alors vous authentifier sur ce fournsseur d'identité pour ouvrir votre session $LLNG_APPNAME.
msg_wsweetEmailNotValid = Your email is not valid
msg_fr_wsweetEmailNotValid = Votre adresse email est invalide
msg_wsweetNoWSweetDomain = No $LLNG_APPNAME domain found for your email
msg_fr_wsweetNoWSweetDomain = Aucun domaine $LLNG_APPNAME trouvé pour votre adresse email
EOF
fi
chmod 640 /etc/lemonldap-ng/lemonldap-ng.ini
